package com.example.sitea;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ActivitiesChildren extends AppCompatActivity {

    private CardView cardLetters, cardColors, cardPuzzle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_children_activities);
        cardLetters = findViewById(R.id.cardLetters);
        cardColors = findViewById(R.id.cardColors);
        cardPuzzle = findViewById(R.id.cardPuzzle);
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        cardLetters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivitiesChildren.this, "Actividad Letras",  Toast.LENGTH_LONG).show();
                Intent letras;
                letras = new Intent(ActivitiesChildren.this, activity_alphabetic.class );
                startActivity(letras);
            }
        });

        cardColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivitiesChildren.this, "Actividad Colores",  Toast.LENGTH_LONG).show();
                Intent view;
                view = new Intent(ActivitiesChildren.this, activity_colors.class );
                startActivity(view);
            }
        });

        cardPuzzle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivitiesChildren.this, "Actividad Rompecabezas",  Toast.LENGTH_LONG).show();
            }
        });
    }
}
