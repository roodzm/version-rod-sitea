package com.example.sitea;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.io.IOException;


public class activiti_songs extends AppCompatActivity {
    MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6;
    CardView CardView1, CardView2, CardView3, CardView4, CardView5, CardView6;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);
        CardView1 = findViewById(R.id.CardView1);
        CardView2 = findViewById(R.id.CardView2);
        CardView3 = findViewById(R.id.CardView3);
        CardView4 = findViewById(R.id.CardView4);
        CardView5 = findViewById(R.id.CardView5);
        CardView6 = findViewById(R.id.CardView6);
        setOnClickListeners();

        mp1=MediaPlayer.create(this,R.raw.raton_vaquero);
        mp1=MediaPlayer.create(this,R.raw.barco_chiquito);
        mp1=MediaPlayer.create(this,R.raw.pin_pon);
        mp1=MediaPlayer.create(this,R.raw.sol_solecito);
        mp1=MediaPlayer.create(this,R.raw.vaca_lola);
        mp1=MediaPlayer.create(this,R.raw.lindo_pescadito);
    }



    private void setOnClickListeners() {
        CardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.raton_vaquero);
                mp1.start();
            }
        });

        CardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.barco_chiquito);
                mp1.start();
            }
        });

        CardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.pin_pon);
                mp1.start();
            }
        });

        CardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.sol_solecito);
                mp1.start();
            }
        });

        CardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.vaca_lola);
                mp1.start();
            }
        });

        CardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activiti_songs.this,R.raw.lindo_pescadito);
                mp1.start();
            }
        });
    }

    protected void onPause(){
        super.onPause();
        mp1.pause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mp1 != null && mp1.isPlaying()) {
            try {
                mp1.stop();
                mp1.prepare();
                mp1.seekTo(0);
                mp1.release();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

