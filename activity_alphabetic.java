package com.example.sitea;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import java.io.IOException;


public class activity_alphabetic extends AppCompatActivity {
    MediaPlayer mp1;
    CardView CardView1, CardView2, CardView3, CardView4, CardView5, CardView6, CardView7, CardView8 ,CardView9 ,CardView10,
     CardView11, CardView12, CardView13, CardView14, CardView15, CardView16, CardView17, CardView18 ,CardView19 ,CardView20,
     CardView21, CardView22, CardView23, CardView24, CardView25, CardView26;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);
        CardView1 = findViewById(R.id.CardView1);
        CardView2 = findViewById(R.id.CardView2);
        CardView3 = findViewById(R.id.CardView3);
        CardView4 = findViewById(R.id.CardView4);
        CardView5 = findViewById(R.id.CardView5);
        CardView6 = findViewById(R.id.CardView6);
        CardView7 = findViewById(R.id.CardView7);
        CardView8 = findViewById(R.id.CardView8);
        CardView9 = findViewById(R.id.CardView9);
        CardView10 = findViewById(R.id.CardView10);
        CardView11 = findViewById(R.id.CardView11);
        CardView12 = findViewById(R.id.CardView12);
        CardView13 = findViewById(R.id.CardView13);
        CardView14 = findViewById(R.id.CardView14);
        CardView15 = findViewById(R.id.CardView15);
        CardView16 = findViewById(R.id.CardView16);
        CardView17 = findViewById(R.id.CardView17);
        CardView18 = findViewById(R.id.CardView18);
        CardView19 = findViewById(R.id.CardView19);
        CardView20 = findViewById(R.id.CardView20);
        CardView21 = findViewById(R.id.CardView21);
        CardView22 = findViewById(R.id.CardView22);
        CardView23 = findViewById(R.id.CardView23);
        CardView24 = findViewById(R.id.CardView24);
        CardView25 = findViewById(R.id.CardView25);
        CardView26 = findViewById(R.id.CardView26);
        setOnClickListeners();


        mp1=MediaPlayer.create(this,R.raw.alphabeta);
        mp1=MediaPlayer.create(this,R.raw.alphabetb);
        mp1=MediaPlayer.create(this,R.raw.alphabetc);
        mp1=MediaPlayer.create(this,R.raw.alphabetd);
        mp1=MediaPlayer.create(this,R.raw.alphabete);
        mp1=MediaPlayer.create(this,R.raw.alphabetf);
        mp1=MediaPlayer.create(this,R.raw.alphabetg);
        mp1=MediaPlayer.create(this,R.raw.alphabeth);
        mp1=MediaPlayer.create(this,R.raw.alphabeti);
        mp1=MediaPlayer.create(this,R.raw.alphabetj);
        mp1=MediaPlayer.create(this,R.raw.alphabetk);
        mp1=MediaPlayer.create(this,R.raw.alphabetl);
        mp1=MediaPlayer.create(this,R.raw.alphabetm);
        mp1=MediaPlayer.create(this,R.raw.alphabetn);
        mp1=MediaPlayer.create(this,R.raw.alphabeto);
        mp1=MediaPlayer.create(this,R.raw.alphabetp);
        mp1=MediaPlayer.create(this,R.raw.alphabetq);
        mp1=MediaPlayer.create(this,R.raw.alphabetr);
        mp1=MediaPlayer.create(this,R.raw.alphabets);
        mp1=MediaPlayer.create(this,R.raw.alphabett);
        mp1=MediaPlayer.create(this,R.raw.alphabetu);
        mp1=MediaPlayer.create(this,R.raw.alphabetv);
        mp1=MediaPlayer.create(this,R.raw.alphabetw);
        mp1=MediaPlayer.create(this,R.raw.alphabetx);
        mp1=MediaPlayer.create(this,R.raw.alphabety);
        mp1=MediaPlayer.create(this,R.raw.alphabetz);
    }



    private void setOnClickListeners() {
        CardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabeta);
                mp1.start();
            }
        });

        CardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetb);
                mp1.start();
            }
        });

        CardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetc);
                mp1.start();
            }
        });

        CardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetd);
                mp1.start();
            }
        });

        CardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabete);
                mp1.start();
            }
        });

        CardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetf);
                mp1.start();
            }
        });

        CardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetg);
                mp1.start();
            }
        });

        CardView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabeth);
                mp1.start();
            }
        });

        CardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabeti);
                mp1.start();
            }
        });

        CardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetj);
                mp1.start();
            }
        });


        CardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetk);
                mp1.start();
            }
        });

        CardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetl);
                mp1.start();
            }
        });

        CardView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetm);
                mp1.start();
            }
        });

        CardView14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetn);
                mp1.start();
            }
        });

        CardView15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabeto);
                mp1.start();
            }
        });

        CardView16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetp);
                mp1.start();
            }
        });

        CardView17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetq);
                mp1.start();
            }
        });

        CardView18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetr);
                mp1.start();
            }
        });

        CardView19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabets);
                mp1.start();
            }
        });

        CardView20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabett);
                mp1.start();
            }
        });


        CardView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetu);
                mp1.start();
            }
        });

        CardView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetv);
                mp1.start();
            }
        });

        CardView23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetw);
                mp1.start();
            }
        });

        CardView24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetx);
                mp1.start();
            }
        });

        CardView25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabety);
                mp1.start();
            }
        });

        CardView26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.stop();
                mp1=MediaPlayer.create(activity_alphabetic.this,R.raw.alphabetz);
                mp1.start();
            }
        });




    }

    protected void onPause(){
        super.onPause();
        mp1.pause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mp1 != null && mp1.isPlaying()) {
            try {
                mp1.stop();
                mp1.prepare();
                mp1.seekTo(0);
                mp1.release();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}