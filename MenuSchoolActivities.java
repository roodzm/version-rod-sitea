package com.example.sitea;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MenuSchoolActivities extends AppCompatActivity {
    Intent songs;
    private CardView cardSongs, cardAssistance, cardRules, cardActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_school_activities);
        cardSongs = findViewById(R.id.cardSongs);
        cardActivity = findViewById(R.id.cardImage);
        cardAssistance = findViewById(R.id.cardAssistance);
        cardRules = findViewById(R.id.cardRules);
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        cardRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MenuSchoolActivities.this, "Reglas de Clase",  Toast.LENGTH_LONG).show();

            }
        });

        cardAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MenuSchoolActivities.this, "Asistencia",  Toast.LENGTH_LONG).show();
            }
        });

        cardActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MenuSchoolActivities.this, "Actividad",  Toast.LENGTH_LONG).show();

            }
        });

        cardSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MenuSchoolActivities.this, "Canciones",  Toast.LENGTH_LONG).show();
                //manager.beginTransaction().replace(R.id.MenuSchoolActivities,fragmentsongs).commit();
                songs = new Intent(MenuSchoolActivities.this, activiti_songs.class);
                startActivity(songs);
            }
        });
    }
}
